// const sortByDisplayOrder = require('./src/utils/sort-by-display-order.js');
// const slidesOrder = require("./src/slides/_order.js");

// const eleventySass = require("@11tyrocks/eleventy-plugin-sass-lightningcss");
const sass = require("sass");
const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");

module.exports = (config) => {
  config.addPlugin(EleventyHtmlBasePlugin);

  // config.addPlugin(eleventySass);
  config.addTemplateFormats("scss");
  config.addExtension("scss", {
    outputFileExtension: "css", // optional, default: "html"

    // `compile` is called once per .scss file in the input directory
    compile: async function (inputContent) {
      let result = sass.compileString(inputContent);

      // This is the render function, `data` is the full data cascade
      return async (data) => {
        return result.css;
      };
    },
  });

  config.addPassthroughCopy("./src/dist");
  config.addPassthroughCopy("./src/styles");
  config.addPassthroughCopy("./src/scripts");
  config.addPassthroughCopy("./src/plugin");
  config.addPassthroughCopy("./src/images");
  config.addPassthroughCopy("./src/assets");

  config.addCollection("slides", (collection) => {
    const slides = collection.getFilteredByGlob("./src/slides/*");

    return [...slides].sort((a, b) => {
      const fa = a.fileSlug.toLowerCase();
      const fb = b.fileSlug.toLowerCase();
      if (fa < fb) return -1;
      if (fa > fb) return 1;
      return 0;
    });
  });

  return {
    markdownTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    htmlTemplateEngine: "njk",

    dir: {
      input: "src",
      output: "public",
    },
  };
};
