<section>
<h3>Siguria digjitale</h3>
...nuk ka të bëjë vetëm me mjetet por dhe me qasjen tonë ndaj informacionit.

<div style="margin: auto; margin-top: 100px;" class="crypto-key"/>

</section>

<section>
<h3 >Siguria digjitale</h3>
... nuk lidhet vetëm me të kuptuarit e riskut, por dhe me hapat që marrim për mitigimin e tij

<div style="margin: auto; margin-top: 100px; transform: scale(-1, 1)" class="crypto-key"/>

</section>

<section>

Gjëra për tu marrë në konsideratë:

- çfarë zgjedhim të ndajmë
- si komunikojmë
- çfarë klikojmë (phishing)
- shërbimet që zgjedhim
- me kë zgjedhim të ndajmë informacion

</section>
