## Velerësimi dhe mitigimi i rrezikut

**Velerësimi i riskut** - hulumtimi mbi dhe listimi i rreziqeve të mundshme që na kanosen gjatë punes kërkimore investigative

**Mitigimi i riskut** - përpjekjet për të minimizuar pasojat negative nga ekspozimi duke marrë masa paraprake
