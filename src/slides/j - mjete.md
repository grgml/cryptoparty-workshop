## Mjete & Teknika

<section>

### Llogaritë

- **llogari të ndara** për aktivitete apo investigime të ndryshme
- nda jetën personale nga puna investigative
- **identitete njëpërdorimshe**

</section>

<section>

### Shfletuesit

- **“privacy aware”** browser
- mos hap email personal ose llogari mediash sociale në të njejtin browser ku po bën kërkime
- kujdes nga **fingerprinting**, mund te gjurmohemi/identifikohemi edhe nese nuk jemi logged-in

</section>
<section>

### VPN & Proxy

- VPN: **enkripton trafikun lokal** në mënyrë që ISP të mos ta lexojë dot
- Proxy: **fsheh ip e perdoruesit** dhe mundëson aksesimin e faqeve të bllokuara nga ISP
- Bitmask, Riseup VPN, PsIPhon, Lantern

</section>
<section>

### Komunikime

**Interceptimi i komunikimeve** - nje nga rreziqet kryesore

- perdore aplikacione për **mesazhe dhe emaile te enkriptuara**
- Kujdes: enkriptimi maskon te dhenat, por jo **metadatat**

</section>
