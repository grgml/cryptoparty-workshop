<div style="display: grid; grid-template-column: repeat(1fr, 3)">

Siguria lidhet me **aktivitetin** që kryejmë dhe **kontekstin** në të cilin e kryejmë

Siguria nuk mund të ndahet në **digjitale dhe jo digjitale**

Siguria nuk mund të shihet në izolim nga **njerezit me të cilët punojmë dhe komunikojmë**

</div>
