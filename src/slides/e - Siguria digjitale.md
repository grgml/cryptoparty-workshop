## Siguria digjitale

2 aspekte të ruajtjes së të dhënave

<ol>
<li><strong>ruajtja e të dhënave nga aktorë të tretë</strong> si dhe nga ekspozimi publik që do përbënte thyerje privatesie  </li>
<li><strong>aftesia për të rekuperuar të dhënat</strong> nëse humbin apo dëmtohen</li>
</ol>
