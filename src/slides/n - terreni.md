<section>

## Siguria në terren

Pse na duhet të dalim në terren, përtej botës digjitale?

- për të mbledhur **prova të pakundërshtueshme**
- informacione jo kollaj të manipulueshme
- **kontakte direkte** me burimet e informacionit

</section>
<section>

### Aftësi që na nevojiten:

- **përshtatja** me situatat
- kuptimi i rrezikut dhe **ndërmjetësimi** apo de-eskalimi i konflikteve të mundshme
- **vlerësimi paraprak** i aftësive dhe mangësive

</section>

<section>

### Rreziqe:

- mjedisore (moti etj)
- terreni
- objekti kërkimor
- ligjore
- siguria personale
- logjistika
- rreziqe të lidhura me provat dhe burimet
</section>
<section>

### Location

I dobishëm për të ndarë informacion, në kohë reale ose jo, por dhe **burim rreziku**
<br />
<br />
<br />

### Metadatat e imazheve

Të dobishme për të **verifikuar autenticitetin** e provave filmike, por mund të **ekspozojnë më shumë se ç'do donim**

</section>
