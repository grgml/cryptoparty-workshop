## Siguria si mendësi

mënyra si i qasemi punës investigative apo aktivizmit

## Siguria si praktikë

mase paraprake dhe ushtrim i vazhdueshëm që na mbron nga rreziku, dhe jo si zgjidhje përfundimtare
