## Pyetje përgatitore

- Çfarë do e bente investigimin e suksesshem?
- Çfarë mund t'ju pengojë nga arritja e objektivit?
- Sa gjasa ka që të ndodhë? Cilat do ishin pasojat?
- Cilat rreziqe duhen prioritizuar?
- Si mund të mitigohen këto rreziqe?
- Çfarë rreziqesh mund të lindin nga masat e marra?
