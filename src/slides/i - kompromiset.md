## Kompromise (trade-offs)

<figure style="">
<img src="images/Triangle of compromise.png" alt="Triangle of compromise" height="300px">
<figcaption>
Fig. Siguria, funksionaliteti, perdorueshmeria - jo gjithmonë mund t'i kemi të treja
</figcaption>
</figure>
