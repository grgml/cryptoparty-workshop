<section>

## Aktivitet praktik

- listoni disa aktivitete që bëni si pjesë e punës investigative
- për çdo aktivitet listoni mjetet, teknikat dhe pajisjet që përdorni
- çfarë masash sigurie ndiqni në secilin rast

<a href="assets/activity-1.odt">Template</a>

</section>

<section>

### Në përfundim:

- krahasoni mjetet dhe teknikat me njëri tjetrin
- gjatë punës kërkimore mund të cenojmë sigurinë personale
- të dhënat që mbledhim mund të përmbajnë informacione mbi burimet tona, ndaj duhet mbajtur të sigurta

</section>
