## Do no harm

Si investigues duhet te synojmë që përmes punës sonë të **maksimizojmë impaktin pozitiv dhe te minimizojme rreziqet** apo efektet negative te mundshme

<!-- **Veprimet dhe sjelljet tona kane pasoja**, pozitive ose negative. -->

**Actions + Behaviours = Consequences**

Synimi është të mos krijojmë më shumë rreziqe apo dëme prej aktivitetit tonë
