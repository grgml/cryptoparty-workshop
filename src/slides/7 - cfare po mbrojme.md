## Çfarë po mbrojmë?

- **kontaktet** - njerëzit të cilët takojmë dhe në të dhënat e të cilëve kemi akses

- **të dhënat** - informacioni qe mbledhim duhet të jetë i sigurtë nga aksesi i paautorizuar dhe i rekuperueshëm në rast dëmtimi apo humbjeje

- **veten** - më e rëndësishmja është të jemi të sigurt dhe të minimizojme ose shmangin rreziqet për veten
