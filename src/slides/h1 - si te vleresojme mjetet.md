<section>

# Si të vlerësojmë mjetet që përdorim?

</section>
<section>

<ul>
<li class="fragment fade-in-then-semi-out">a është <strong>open-source</strong> (me kod burim të hapur) </li>
<li class="fragment fade-in-then-semi-out">i besueshëm dhe i <strong>audituar</strong> </li>
<li class="fragment fade-in-then-semi-out">a është i qendrueshëm dhe a ka <strong>komunitet</strong> per të bërë pyetje dhe ndarë njohuri</li>
<li class="fragment fade-in-then-semi-out"><strong>enkriptimi E2E</strong></li>
<li class="fragment fade-in-then-semi-out"><strong>nuk ruan të dhëna</strong> të panevojshme</li>
<li class="fragment fade-in-then-semi-out"><strong>nuk ndan të dhëna</strong> me palë të treta</li>
<li class="fragment fade-in-then-semi-out"><strong>dokumentacion</strong> publik i aksesueshëm </li>
<li class="fragment fade-in-then-semi-out">suportohet nga <strong>platforma të ndryshme</strong> </li>
<li class="fragment fade-in-then-semi-out"><strong>user friendly</strong></li>
</ul>
</section>
