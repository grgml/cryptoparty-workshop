<section>

# Praktika të mira:

</section>

<section>

- fjalëkalime të gjata
- menaxhues fjalëkalimesh
- **2FA** - autorizim i dyfishtë (me dy faktore)
- sigurohu të kesh mënyra për **recovery**, (p.sh. recovery email)
- mbaj **backups**
- **enkripto** të dhënat dhe backupet
- ji i ndërgjegjshëm se kush i ka të dhënat e tua
- vlëreso mjetet që perdor
</section>
