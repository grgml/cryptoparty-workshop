<h4 style="text-align: start"> Burime: </h4>

- [Exposing the invisible](https://exposingtheinvisible.org/en/)
- [Exposing the invisible - The Kit](https://kit.exposingtheinvisible.org/en/index.html)
- [The Holistic Security Manual](https://holistic-security.tacticaltech.org/)
- [Surveillance Self-Defense](https://ssd.eff.org/)
- [Security in a box](https://securityinabox.org/en/)
- [Data detox kit](https://datadetoxkit.org/en/home)
- [Risk Analysis & Protection Planning - Front Line Defenders](https://www.frontlinedefenders.org/en/programme/risk-analysis-protection-training)
