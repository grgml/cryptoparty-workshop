## Synimet e punëtorisë

1. Të trajtojmë **parimet kryesore të sigurisë digjitale** dhe hapat që duhet të ndjekim para se të nisim një investigim
2. Të kuptojmë se si **gjurmët digjitale mund të ekspozojnë informacion** dhe të na vënë në rrezik
