<section>

## Aktivitet praktik

- përcakto objektin e investigimit
- identifiko aktivitetet, skenarët e suksesit dhe rreziqet potenciale
- prioritizo rreziqet duke vlerësuar gjasat dhe pasojat; vendosi ne matricen e vlerësimit të rrezikut
- për secilin rrezik, përshkruaj masa mitiguese, si dhe rreziqe të reja që mund të lindin prej tyre

<a href="assets/activity-2.odt">Template</a>

</section>
<section>

### Përfundime

- rreziku "trashëgohet" nga personat e kontaktit
- analiza e rreziqeve është aparat përgatitor dhe jo si "checklist"
- analiza e rreziqeve ësthë proces iterativ
</section>
